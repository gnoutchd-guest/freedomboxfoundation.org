# Join the Discussion

There are a variety of ways to keep up with project information and
even more ways to contribute.

Announcements will be made at
[http://freedomboxfoundation.org](http://freedomboxfoundation.org),
and there is of course an [rss
feed](http://www.freedomboxfoundation.org/index.rss).

You can discuss the project on the [Debian FreedomBox mailing
list](http://lists.alioth.debian.org/mailman/listinfo/freedombox-discuss),
the [Debian wiki](http://wiki.debian.org/FreedomBox) and in
`#freedombox` on `irc.oftc.net`.  We spend time in the discussion
venues and try to participate as time allows.

If you want to contact the FreedomBox Foundation itself, see the [[contact]] page.
