[[!meta title="Privacy Policy"]]

# Privacy Policy

The FreedomBox Foundation is committed to protecting the privacy of our website visitors and our supporters. In this policy, “the FreedomBox Foundation” or “We” refers to the FreedomBox Foundation Board of Directors, Management, staff, interns, volunteers, and consultants. As to all of the information described below, the FreedomBox Foundation will not give, sell, rent, or exchange the information with anyone else without your prior consent, except as compelled by law (see section on Compelled Disclosure below).

Information Collected
---------------------

When you visit our site, the FreedomBox Foundation may record the numerical Internet protocol address of the computer you are using, the browser software you use and your operating system, the date and time you access our site, and the Internet address of the website from which you linked directly to our site, in addition to other information. We use this information to measure the number of visitors to different sections of the site, to diagnose and correct system errors, and to improve the site. When you communicate with us through our site or third-party donation services, we may collect your name, email address, postal address, and other personal, financial, legal or technical information.

**Cookies**: We do not use cookies on this website. However, when you make a donation through one of our supported third-party donation services (e.g. PayPal, Network for Good, etc.), you may be directed to websites that do use cookies. Please read the privacy policies of those websites for more information. We do not govern the privacy policies or data protection practices of the third-party donation services which we use to collect donations.

**Voluntarily Submitted Information**: We collect information that you voluntarily submit to us, including identifying information and contact information. We may collect voluntarily submitted information in many settings, including but not limited to in-person interactions at public events, paper sign-up sheets, donation transactions, our official social media pages, and user feedback. Please note that personally identifying information may be required if you want to donate through PayPal or Network for Good.

**Mailing Lists**: The FreedomBox Foundation maintains one mailing list without the use of a third-party service. We manage our mailing list internally and host the mailing list software on our own server. You can subscribe to our mailing list via email or by writing your email address on a paper form at a public event. You can unsubscribe from our mailing list at any time using the instructions at the bottom of every email we send to the mailing list. These instructions are as follows: to unsubscribe, send an empty email to <announce+unsubscribe@lists.freedombox.org>.  This will also prompt a confirmation email. 

If you want to learn which of your email addresses we have on file and ask us to delete, update, or change it, please email <privacy@freedomboxfoundation.org>.

Note that the FreedomBox community manages its own mailing list [here](https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/freedombox-discuss).
This mailing list is not managed or governed by the FreedomBox Foundation, and our privacy policy therefore does not apply to the FreedomBox community mailing list.

**Sharing Information with Third-Parties**: You share voluntarily submitted information with any third-party donation service that you use to make a donation (e.g. Paypal). We may be required to share your voluntarily submitted information with relevant third-parties in the course of an audit. We do not share your voluntarily submitted information with any companies with which we may work.

**Why We Collect Information**: In most cases, the FreedomBox Foundation collects information in order to perform basic non-profit operations like collecting donations, sending emails to our supporters, and providing support to users. If you want to learn more about why we collect information, please email us at <privacy@freedomboxfoundation.org>.

Securing Your Information
---------------------

The FreedomBox Foundation takes many measures to secure and protect the information we collect. Our measures include encrypting data, using the HTTPS communication protocol on our website, password-protecting our storage, and hosting and administering our own servers on premises we control.

General Data Protection Regulation (“GDPR”)
---------------------

The FreedomBox Foundation is required to comply with the GDPR because we (1) offer products or services to citizens of the European Union and (2) collect personal information from citizens of the European Union.

Managing Your Information
---------------------

You may change, update, access, or delete the information you have submitted to us by sending an email to <privacy@freedomboxfoundation.org>. You may also send an email to this address to withdraw your consent or restrict our use and/or processing of your personal information. Additionally, you may submit a subject access request to this email address in order to receive a copy of any personal data pertaining to you.

Compelled Disclosure
--------------------

If we are required by law to disclose any of the information collected about you, we will attempt to provide you with notice (unless we are prohibited) that a request for your information has been made in order to give you an opportunity to object to the disclosure. We will attempt to provide this notice by email if you have given us an email address, or by postal mail if you have provided a postal address. We will independently object to overly broad requests for access to information about users of our site. If you do not challenge the disclosure request, we may be legally required to turn over your information.

Contacting the FreedomBox Foundation
---------------------

If you have any questions about our privacy policy and data protection practices, you can reach us by email at <privacy@freedomboxfoundation.org> or by postal mail at the following address:

>FreedomBox Foundation  
>33 West 60th Street, FL 2  
>New York, NY 10023  



**Date Updated**: Privacy Policy most recently updated on March 26, 2019.
