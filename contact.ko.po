# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
#   <clint@debian.org>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2018-01-05 22:17+0000\n"
"PO-Revision-Date: 2011-05-16 21:19+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"

#. type: Title #
#, no-wrap
msgid "Contact"
msgstr ""

#. type: Plain text
msgid ""
"If you are interested in joining the ongoing discussion about FreedomBox "
"with users and developers, sign up for our [Mailing list](http://lists."
"alioth.debian.org/mailman/listinfo/freedombox-discuss).  For technical "
"support, please refer to the same mailing list. If you would like to contact "
"the FreedomBox Foundation directly, our contact information is below."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Email"
msgstr ""

#. type: Plain text
msgid "General information <info@freedomboxfoundation.org>"
msgstr ""

#. type: Plain text
msgid "Press inquires <press@freedomboxfoundation.org>"
msgstr ""

#. type: Plain text
msgid ""
"If you are interested in supporting the FreedomBox project's efforts, either "
"as a developer or future user, please write to us and let us know. "
"<join@freedomboxfoundation.org>"
msgstr ""

#. type: Plain text
msgid ""
"If you are interested in making a financial contribution to the Foundation, "
"please write to <donate@freedomboxfoundation.org>"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Mailing Address"
msgstr ""

#. type: Plain text
msgid "Freedom Box Foundation 1995 Broadway FL17 New York, NY 10023"
msgstr ""
