## FreedomBox 0.9 Released

*April 25th, 2016*

We're pleased to announce that FreedomBox 0.9 has been released! This release comes 9 weeks after the previous release (0.8).

The FreedomBox packages are available in Debian testing: freedombox-setup 0.9 and plinth 0.9.1-1.

We are planning to build new images for this release. However, there is an issue with building images for armel/armhf boards at the moment. We should have a workaround for this issue soon, and then the images will be built and uploaded.

More information on this release is available on the wiki:

<https://wiki.debian.org/FreedomBox/ReleaseNotes>

Major FreedomBox 0.9 Changes:

- Fixed Wi-Fi AP setup. 
- Prevent lockout of users in 'sudo' group after setup is complete. 
- Improved setup mechanism for Plinth modules. Allows users to see what a module is useful for, before doing the setup and package install. Also allows essential modules to be setup by default during FreedomBox install.
- Added HTTPS certificates to Monkeysphere page. Reorganized so that multiple domains can be added to a key.
- Added Radicale, a CalDAV and CardDAV server.
- Added Minetest Server, a multiplayer infinite-world block sandbox.
- Added Tiny Tiny RSS, a news feed reader.

Known Issues:

- ownCloud has been removed from Debian testing, and is not installable through Plinth.
  - If you are using ownCloud on FreedomBox, please know that it will not be maintained.
  - We are looking for applications to replace the main functionality that was provided by ownCloud.
  - Radicale is an alternative for the contact and calendar sync features.
  - We still need to find and integrate a good alternative for the file sync feature.

Thanks to all who helped to put this release together!

Please feel free to join us to discuss this release on the mailing list, IRC, or on the monthly progress calls:

- List: <http://lists.alioth.debian.org/pipermail/freedombox-discuss/>

- IRC: <irc://irc.debian.org/freedombox>

- Calls: <https://wiki.debian.org/FreedomBox/ProgressCalls>
