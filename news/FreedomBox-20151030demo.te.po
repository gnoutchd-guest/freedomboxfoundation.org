# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-12-23 17:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "##Video of Freedombox Demo v0.6 posted\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "*December 23, 2015*\n"
msgstr ""

#. type: Plain text
msgid ""
"You can find video of Sunil's demo of the 0.6 release of the FreedomBox "
"software at the Software Freedom Law Center's video [archive.](https://"
"softwarefreedom.org/events/2015/conference/video.html)"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"We have released a newer version of FreedomBox since then that primarily focused\n"
" on internationalization. If you are interested in downloading the newest versi\n"
"on you can find the official images [here.](http://ftp.skolelinux.org/pub/freedombox/latest/)\n"
msgstr ""
