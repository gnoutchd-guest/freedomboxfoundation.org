[[!meta title="What Is A Distributed Social Network?"]]

# What Is A Distributed Social Network?

J David Eisenberg made an excellent comic [introduction to distributed social networks](http://dsn-test.com/dsn-vn/).  For anybody who isn't quite sure why the FreedomBox is important, that's a fun and non-technical way to explain it.

[[!sidebar  content="""

Links:
[[Home|https://www.freedomboxfoundation.org/]]  
[[FAQ]]  
[[Donate]]

"""]]
