# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# iLennart21 <a12s34d56f78@live.com>, 2012
# schulmar <schulmar@hrz.tu-chemnitz.de>, 2012
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2016-02-24 17:43+0000\n"
"PO-Revision-Date: 2014-11-25 21:23+0000\n"
"Last-Translator: fbfwiki <root@softwarefreedom.org>\n"
"Language-Team: German (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/de/)\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"FreedomBox Update\"]]\n"
msgstr "[[!meta title=\"FreedomBox Update\"]]\n"

#. type: Title #
#, no-wrap
msgid "FreedomBox Update After DebConf"
msgstr "FreedomBox Update nach der DebConf"

#. type: Plain text
#, no-wrap
msgid ""
"Many hackers involved in FreedomBox had the chance to meet in Banja Luka\n"
"at <a href=\"http://debconf11.debconf.org/\">DebConf11</a>. Bdale Garbee \n"
"gave a speech highlighting the status of the development. The full \n"
"recording of the session is available on <a href=\"http://penta.debconf.org/dc11_schedule/events/704.en.html\">\n"
"Debian's site</a>. If you already know the \n"
"basic of FreedomBox project, skip to minute 33 to hear the latest \n"
"development and the next steps.\n"
msgstr "Viele Hacker, die sich an FreedomBox beteiligen, hatten die Chance sich in Banja Luka auf der <a href=\"http://debconf11.debconf.org/\">DebConf11</a> zu treffen. Bdale Garbee gab eine Rede, die den Fortschritt der Entwicklung betonte. Die ganze Aufnahme der Tagung ist auf der <a href=\"http://penta.debconf.org/dc11_schedule/events/704.en.html\">Debian Seite</a> verfügbar. Wenn du bereits die Grundlagen des FreedomBox Projektes kennst, spule zur 33. Minute um die letzten Entwicklungen und die nächsten Schritte zu erfahren.\n"

#. type: Plain text
#, no-wrap
msgid "[[!sidebar  content=\"\"\"\n"
msgstr "[[!sidebar  content=\"\"\"\n"

#. type: Plain text
#, no-wrap
msgid ""
"Links:\n"
"[[Home|https://www.freedomboxfoundation.org/]]  \n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[FAQ]]  \n"
msgstr ""

#. type: Plain text
msgid "[[Donate]]"
msgstr "[[Spenden]]"
