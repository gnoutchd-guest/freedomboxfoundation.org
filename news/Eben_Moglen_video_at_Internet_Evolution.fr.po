# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# larousteauchat <guillaume.rabasse@gmail.com>, 2011
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2016-02-24 17:43+0000\n"
"PO-Revision-Date: 2012-02-14 14:13+0000\n"
"Last-Translator: fbfwiki <root@softwarefreedom.org>\n"
"Language-Team: French (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/fr/)\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: Plain text
#, no-wrap
msgid "#Eben Moglen video at Internet Evolution \n"
msgstr "#Eben Moglen vidéo à Internet Evolution\n"

#. type: Plain text
msgid ""
"While at Personal Democracy Forum last week, Eben was interviewed by Nicole "
"Ferraro of Internet Evolution. The first part of that video, focusing on "
"defining what the FreedomBox is, has now been put online here: [Internet "
"Evolution](http://www.internetevolution.com/video.asp?"
"section_id=1361&doc_id=207273)."
msgstr ""
"Au Personal Democracy Forum, la semaine dernière, Eben a été interviewé par "
"Nicole Ferraro de Internet Evolution. La première partie de cette vidéo, se "
"concentrant sur la définition de ce que le FreedomBox est, a été mis en "
"ligne ici: [Internet Evolution](http://www.internetevolution.com/video.asp?"
"section_id=1361&amp;doc_id=207273)."

#. type: Plain text
msgid "Further videos still to come next week."
msgstr "D'autres vidéos à venir la semaine prochaine."
