# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2016-02-24 17:43+0000\n"
"PO-Revision-Date: 2014-11-25 21:35+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Dutch (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/nl/)\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Plain text
msgid ""
"FreedomBox, OpenITP, InformSec and ISOC-NY have partnered up to host a "
"circumvention tools hackfest in NYC right before HOPE. We've got four days "
"to plan, code and learn! If you want to hack on anti-censorship or anti-"
"surveillance tools, bring your project, bring your skills and bring your "
"friends. This event will be focused on writing code and solving design "
"problems. We won't have any long presentations (there will be enough of "
"those at HOPE), though we will have lightning talks and will give away a "
"door prize or two."
msgstr ""

#. type: Plain text
msgid "Where: Columbia Law School, Jerome Greene Hall, 116th and Amsterdam"
msgstr ""

#. type: Plain text
msgid "When: July 9 - 12, 10 am"
msgstr ""

#. type: Plain text
msgid "Who: Privacy and free communication hackers like you"
msgstr ""

#. type: Plain text
msgid ""
"Please RSVP to kaurin at openitp.org and tell us what you plan to work on, "
"what kind of projects and people you hope to meet, and which days you will "
"join us."
msgstr ""

#. type: Plain text
msgid "Feel free to repost this invite or to link to it."
msgstr ""

#. type: Plain text
msgid ""
"Some modest travel stipends are available for amazing projects. Email James "
"Vasile (james at openitp.org) about those."
msgstr ""

#. type: Plain text
msgid ""
"Some projects we know will attend: Commotion Wireless, Cryptocat, Guardian "
"Project, the Lantern Project, and Access."
msgstr ""

#. type: Plain text
msgid ""
"If you are looking for lodging, take a look at this list of nearby hotels. "
"If you want a hostel, there's one on 103rd and Amsterdam."
msgstr ""

#. type: Plain text
msgid ""
"Big thanks to our partners, all of whom are contributing crucial support and "
"resources."
msgstr ""
