[[!meta title="Launching Sales of Pioneer Edition FreedomBox"]]
# Announcing the Launch of Sales of Pioneer Edition FreedomBox Home Servers
## April 22nd, 2019

The FreedomBox Foundation is proud to announce that sales of the first commercially available FreedomBox have launched. Orders can be placed [here.](https://www.olimex.com/Products/OLinuXino/Home-Server/Pioneer-FreedomBox-HSK/)

The product, dubbed a “Pioneer Edition FreedomBox Home Server Kit,” is being sold by [Olimex](https://www.olimex.com/), a company which specializes in [Open Source Hardware](https://en.wikipedia.org/wiki/Open-source_hardware). The product includes pocket-sized server hardware, an SD card with the operating system pre-installed, and a backup battery which can power the hardware for 4-5 hours in case of outages. It sells for €82 and ships globally.

The FreedomBox community will be offering free technical support for owners of the Pioneer Edition FreedomBox servers on our [support forum](https://discuss.freedombox.org/c/pioneer-support). The only thing users pay for is hardware.

FreedomBox is a small private server that was originally conceived in 2010 as an alternative to Facebook and other social media platforms. In the nine years since, a global consensus has emerged about centralized social media platforms: they are a threat to privacy and democracy. FreedomBox will be delivered to doorsteps just as people realize that they’ve needed it all along.

FreedomBox is designed around the principle that the exploitation of user data and attention should be technologically impossible. To that end, it is a user-controlled device that enables almost anyone to decentralize the web by hosting their own corner of the internet at home. Its simple user interface empowers individuals to host their own Internet services without any expertise, like an encrypted chat server that can replace Whatsapp, a VoIP server, a personal website, file sharing, a metasearch engine, and much more. The FreedomBox software is fully free and open source, and it is supported by the non-profit FreedomBox Foundation.

## Try Before You Buy

In anticipation of the launch of sales, the FreedomBox team recently created a live demo of FreedomBox. Anyone can demo the software to learn what they will be getting in the product. [Click here to try our free demo.](https://freedombox.org/demo/)

## How We Got Here

In 2010, Eben Moglen, Professor of Law at Columbia Law School, delivered a speech called “Freedom in the Cloud” in which he made a prescient claim: “Mr. Zuckerberg has attained an unenviable record: he has done more harm to the human race than anybody else his age.” <sup>[1](https://www.softwarefreedom.org/events/2010/isoc-ny/FreedomInTheCloud-transcript.html)</sup> In this speech, Professor Moglen predicted the damage that would be done by Facebook; and in his next breath, he conceived of an effort to create an alternative Internet: FreedomBox.

On February 17, 2011, a Kickstarter campaign was launched to fund the project. It quickly raised $86,724. In 2011 and 2012, FreedomBox attracted much attention in the press, and it was viewed as a high-potential emerging technology by writers for the New York Times and Wall Street Journal, among others. <sup>[2](https://www.nytimes.com/2011/02/16/nyregion/16about.html?mtrref=undefined)</sup><sup>,</sup> <sup>[3](https://blogs.wsj.com/digits/2011/02/16/freedom-box-needs-a-good-user-interface/)</sup>

After 2012, the project went through a stage of quiet development. But by 2017, the private sector global technology company ThoughtWorks had hired two developers in India to work on FreedomBox full-time. These two full-time developers plus a solid team of about 10 volunteer developers built a thriving software ecosystem around the project.

FreedomBox has grown an impressive presence in India. In fact, 12 villages in rural India rely on FreedomBox for digital services, thanks to the work of its advocates. In February 2019, the Times of India even published a national story about FreedomBox in rural India. <sup>[4](https://timesofindia.indiatimes.com/entertainment/events/hyderabad/every-old-computer-is-a-potential-server-that-can-bring-internet-connectivity-to-an-entire-village/articleshow/67833580.cms)</sup>

FreedomBox has succeeded in growing a thriving software ecosystem. Now, the FreedomBox Foundation is bringing FreedomBox to the hardware market. The Foundation is proud to partner with Olimex, a well respected company in the world of free and open source software. The word "Pioneer" was included in the name of these kits in order to emphasize the leadership required to run a FreedomBox in 2019. Users will be pioneers both because they have the initiative to define this new frontier and because their feedback will make FreedomBox better for its next generation of users.

The Pioneer FreedomBox Home Server Kits can be purchased here: <https://www.olimex.com/Products/OLinuXino/Home-Server/Pioneer-FreedomBox-HSK/>

Sources: 

[1] <https://www.softwarefreedom.org/events/2010/isoc-ny/FreedomInTheCloud-transcript.html>

[2] <https://www.nytimes.com/2011/02/16/nyregion/16about.html?mtrref=undefined>

[3] <https://blogs.wsj.com/digits/2011/02/16/freedom-box-needs-a-good-user-interface/>

[4] <https://timesofindia.indiatimes.com/entertainment/events/hyderabad/every-old-computer-is-a-potential-server-that-can-bring-internet-connectivity-to-an-entire-village/articleshow/67833580.cms>