[[!meta title="Supporting Two New Hardware Models"]]
# Supporting Two New Hardware Models
## May 28th, 2019

We are pleased to announce that FreedomBox now supports two new hardware models: the Banana Pro™ single-board computer and the Pine64(+) single-board computer. This means that the FreedomBox team will publish operating system images for each of these two boards whenever a new version of FreedomBox is released. For now, images for these two boards are only available for the [Testing](https://freedombox.org/download/testing/) and [Nightly](https://freedombox.org/download/nightly/) versions of FreedomBox, but images will be available for the Stable version soon. Download the images [here](https://freedombox.org/download/).

## About the Banana Pro™

The [Banana Pro™](http://www.lemaker.org/product-bananapro-specification.html) is an updated version of the well known [Banana Pi™](https://en.wikipedia.org/wiki/Banana_Pi) single-board computer. The Banana Pro™ is widely used for free and open source software, and it enjoys compatibility with many Linux-based systems. 

Technical specifications of the Banana Pro™ board are below:

* **Chip:** Allwinner® A20(sun 7i)
* **CPU:** ARM® Cortex™-A7 Dual-Core
* **RAM:** 1GB DDR3 SDRAM
* **Storage:** MicroSD (TF) card slot and SATA 2.0 (suggest 2.5 inch SSD or HDD)
* **Ethernet Port:** Gigabit Ethernet (10/100/1000Mbps) 
* **Wireless:** WiFi 802.11 b/g/n, 2.4GHz
* **USB:** 2 x USB2.0 Host, 1 x USB 2.0 OTG (all direct from A20 chip)
* **Power Source:** 5V @ 2A via MicroUSB connector

Full technical specifications can be found [here](http://www.lemaker.org/product-bananapro-specification.html).

## About the Pine64(+)

The [Pine64(+)](https://www.pine64.org/devices/single-board-computers/pine-a64/) is the first 64-bit single-board computer made by [Pine64](https://en.wikipedia.org/wiki/Pine64). It is one of the more high-powered hardware models supported by FreedomBox, and a thriving community of Pine64 enthusiasts build operating system images for this single-board computer. 

Technical specifications of the Pine64(+) board are below:

* **Chip:** Allwinner® A64 Quad Core
* **CPU:** Quad-core ARM® Cortex-A53 Processor@1.152Ghz
* **RAM:** Variants offered in 512MB, 1GB, and 2GB DDR3 (Both 1GB and 2GB versions of Pine A64+ are supported with the same FreedomBox image)
* **Storage:** MicroSD card slot
* **Ethernet Port:** Gigabit Ethernet (10/100/1000Mbps)
* **Wireless:** WiFi 802.11 b/g/n (as an optional expansion module)
* **USB:** 2 x USB2.0 Host
* **Power Source:** DC 5V @ 2A, 3.7V Li-Ion battery connector via MicroUSB connector or Euler connector

Full technical specifications can be found [here](https://wiki.pine64.org/index.php/PINE_A64_Main_Page).

## Thanks

With these two new models, the total number of hardware models for which the FreedomBox team creates custom images has grown to 12. Credit goes to Sunil Mohan Adapa and Joseph Nuthalapati for creating FreedomBox images for the Banana Pro™ and Pine64(+) boards.

*Banana Pro™ and Banana Pi™ are trademarks of LeMaker.*

