# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-12-20 16:35+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "##FreedomBox 0.7 Released!\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "*December 19, 2015*\n"
msgstr ""

#. type: Plain text
msgid ""
"I'm pleased to announce that FreedomBox 0.7 has been released! This release "
"comes 7 weeks after the previous release (0.6)."
msgstr ""

#. type: Plain text
msgid "FreedomBox version 0.7 is available here:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "    http://ftp.skolelinux.org/pub/freedombox/0.7/\n"
msgstr ""

#. type: Plain text
msgid ""
"Before using, you should verify the image's signature. See https://wiki."
"debian.org/FreedomBox/Download for further instructions."
msgstr ""

#. type: Plain text
msgid "Thanks to all who helped to put this release together."
msgstr ""

#. type: Plain text
msgid "More information on this release is available on the wiki:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "    https://wiki.debian.org/FreedomBox/ReleaseNotes\n"
msgstr ""

#. type: Plain text
msgid "Major FreedomBox 0.7 Changes:"
msgstr ""

#. type: Plain text
msgid ""
"- Translations! Full translations of the interface in Danish, Dutch, French, "
"German and Norwegian Bokmål, and partial Telugu.  - Support for OLinuXino "
"A20 MICRO and LIME2 - New Plinth applications: OpenVPN, reStore (currently "
"disabled in Plinth config, until Debian package is uploaded)  - Improved "
"first-boot experience - Many bugfixes and cleanups"
msgstr ""

#. type: Plain text
msgid "Known Bugs:"
msgstr ""

#. type: Plain text
msgid ""
"- When Transmission page is accessed after install, it will show \"403: "
"Forbidden\". Here is a workaround for this issue:"
msgstr ""

#. type: Bullet: '1. '
msgid "Log into your FreedomBox using the console or SSH."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"2. Edit the Transmission configuration:\n"
"$ sudo vi /etc/transmission-daemon/settings.json\n"
"Change \"rpc-whitelist-enabled\" to false. Save and quit.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"3. Reload the Transmission server.\n"
"$ sudo invoke-rc.d transmission-daemon reload\n"
"*** Important: Make sure you *reload* instead of *restart* the service.\n"
"Otherwise your changes to the configuration will get overwritten.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"4. Access the Transmission page at https://freedombox.local/transmission\n"
"or similar.\n"
msgstr ""

#. type: Plain text
msgid ""
"Please feel free to join us to discuss this release on the mailing list, "
"IRC, or on the monthly progress calls:"
msgstr ""

#. type: Plain text
msgid "- List: http://lists.alioth.debian.org/pipermail/freedombox-discuss/"
msgstr ""

#. type: Plain text
msgid "- IRC: irc://irc.debian.org/freedombox"
msgstr ""

#. type: Plain text
msgid "- Calls: https://wiki.debian.org/FreedomBox/ProgressCalls"
msgstr ""
