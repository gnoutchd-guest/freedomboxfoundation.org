# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Veeven  <veeven@gmail.com>, 2011
# వీవెన్ <veeven@gmail.com>, 2012
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2015-01-08 17:47+0000\n"
"PO-Revision-Date: 2013-04-17 08:20+0000\n"
"Last-Translator: fbfwiki <root@softwarefreedom.org>\n"
"Language-Team: Telugu (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/te/)\n"
"Language: te\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Title #
#, no-wrap
msgid "Thanks"
msgstr "కృతజ్ఞతలు"

#. type: Plain text
msgid ""
"The FreedomBox is a project built on the generous contributions of many "
"volunteers.  This page highlights some of those volunteers.  Of course it "
"could never completely capture the efforts of all our many contributors."
msgstr ""
"చాలా మంది ఔత్సాహికుల ఉదార కృషిపై ఫ్రీడమ్‌బాక్స్ ప్రాజెక్టు నిర్మితమైంది.  ఈ పేజీ వారిలో కొంత మంది ఔత్సాహికులను "
"ప్రదర్శిస్తుంది.  కానీ చాలా మంది మా తోడ్పాటుదార్ల కృషిని పూర్తిగా ఎప్పటికీ ప్రతిఫలించలేకపోవచ్చు."

#. type: Plain text
msgid ""
"Aaron Williamson designed the visual look of this website.  It is a great "
"improvement over the previous lack of design."
msgstr ""
"ఈ వెబ్ సైటు యొక్క దృశ్యరూపాన్ని ఆరన్ విలియమ్సన్ రూపొందించాడు.  అసలు డిజైనే లేని ముందు స్థితి కంటే ఇది "
"చాలా మెరుగు."

#. type: Plain text
msgid ""
"Luka Marčetić designed the [[GNU-in-a-box logo|http://www."
"freedomboxfoundation.org/images/freedombox_large.png]]."
msgstr ""
"లూకా మార్కెటిక్ [[పెట్టెలో-గ్నూ చిహ్నాన్ని|http://www.freedomboxfoundation.org/images/"
"freedombox_large.png]] రూపొందించాడు."

#~ msgid ""
#~ "Jack Edge started the development of [[ManusVexo|https://gitorious.org/"
#~ "manusvexo/manusvexo]]"
#~ msgstr ""
#~ "జాక్ ఎడ్జ్ [[ManusVexo|https://gitorious.org/manusvexo/manusvexo]] అభివృద్ధిని "
#~ "మొదలుపెట్టాడు."
