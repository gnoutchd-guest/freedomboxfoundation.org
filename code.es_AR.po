# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# panic <asimetriaperfecta@hotmail.es>, 2012
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2018-01-05 22:17+0000\n"
"PO-Revision-Date: 2014-11-25 21:35+0000\n"
"Last-Translator: fbfwiki <root@softwarefreedom.org>\n"
"Language-Team: Spanish (Argentina) (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/es_AR/)\n"
"Language: es_AR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "Image"
msgid "#Image\n"
msgstr "Imagen "

#. type: Plain text
msgid "Test text!"
msgstr ""

#. type: Plain text
msgid ""
"A copy of the binary image for several hardward platforms are currently "
"avialble. Freedombox is based on Debian Linux so a wide variety of platforms "
"are supported. The FreedomBox Development team provides images for the most "
"popular platforms currently used by the members of the team."
msgstr ""

#. type: Plain text
msgid "Currently the FreedomBox development team supports:"
msgstr ""

#. type: Bullet: '* '
msgid "VirtualBox (should work with other virtual machine hosts)"
msgstr ""

#. type: Bullet: '* '
msgid "RaspberryPi"
msgstr ""

#. type: Bullet: '* '
msgid "BeagleBone"
msgstr ""

#. type: Bullet: '* '
msgid "DreamPlug"
msgstr ""

#. type: Plain text
msgid "Images: <http://ftp.skolelinux.org/pub/freedombox/>"
msgstr ""

#. type: Plain text
msgid ""
"Instructions for [[downloading and verifying images|https://wiki.debian.org/"
"FreedomBox/Download]] are avaliable."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Plinth"
msgstr "Plinth"

#. type: Plain text
msgid ""
"Our configuration front-end is Plinth, a pluggable web. Plinth comes "
"installed in [[images|https://wiki.debian.org/FreedomBox/Download]] of "
"FreedomBox."
msgstr ""

#. type: Plain text
msgid ""
"You can also install it on Debian sid (unstable). To install plinth run:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "    $sudo apt-get install plinth\n"
msgstr ""

#. type: Plain text
msgid "Source code: <https://github.com/freedombox/Plinth>"
msgstr ""

#. type: Plain text
msgid ""
"Plinth is [Free Software](https://gnu.org/philosophy) licensed under [GNU "
"Affero General Public License](https://www.gnu.org/licenses/agpl.html) "
"version 3 or (at your option) a later version."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Freedom-Maker"
msgstr ""

#. type: Plain text
msgid ""
"Freedom Maker is a script to build FreedomBox disk images for use on various "
"hardware devices or virtual machines. Freedom Maker can currently build disk "
"images for:"
msgstr ""

#. type: Bullet: '* '
msgid "Cubietruck"
msgstr ""

#. type: Plain text
msgid ""
"Source code: <https://alioth.debian.org/anonscm/git/freedombox/freedom-maker."
"git/>"
msgstr ""

#. type: Plain text
msgid ""
"Visit the wiki to [[learn|https://wiki.debian.org/FreedomBox/Maker]] more "
"about contributing or using Freedom-Maker"
msgstr ""

#. type: Plain text
msgid ""
"Freedom Maker is [Free Software](https://gnu.org/philosophy) licensed under "
"[GNU General Public License](https://www.gnu.org/licenses/gpl.html) version "
"3 or (at your option) a later version."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "##FreedomBox-Setup\n"
msgstr ""

#. type: Plain text
msgid ""
"FreedomBox setup is a Debian package that turns a Debian box into a "
"FreedomBox by setting up basic networking, webserver, user accounts, and "
"essential packages. It is currently only available in Debian Sid (unstable). "
"To install FreedomBox setup run:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "    $ sudo apt-get install freedombox-setup\n"
msgstr ""

#. type: Plain text
msgid ""
"Source code: <https://alioth.debian.org/anonscm/git/freedombox/freedombox-"
"setup.git/>"
msgstr ""

#. type: Plain text
msgid ""
"FreedomBox Setup is [Free Software](https://gnu.org/philosophy) licensed "
"under [GNU Affero General Public License](https://www.gnu.org/licenses/agpl."
"html) version 3 or (at your option) a later version."
msgstr ""

#~ msgid "Remove the power supply and rubber feet from your FreedomBox."
#~ msgstr "Eliminar el cargador los pies de goba de su FreedomBox."

#~ msgid "Reinsert the card and reassemble the box"
#~ msgstr "Reinsiterte la tarjeta y cierre la caja"

#~ msgid "GPG"
#~ msgstr "GPG"

#~ msgid "MD5"
#~ msgstr "MD5"

#~ msgid ""
#~ "<table>\n"
#~ "    <tr>\n"
#~ "        <td>freedombox_14_nov_2011.img.xz</"
#~ "td><td>23f83558b41ed2617c741ca7c866b63f</td>\n"
#~ "    </tr>\n"
#~ "    <tr>\n"
#~ "        <td>freedombox_14_nov_2011.img.xz.sig</"
#~ "td><td>404fd39a1ad488b9d3662628a3b5a43b</td>\n"
#~ "    </tr>\n"
#~ "</table>\n"
#~ msgstr ""
#~ "<table>\n"
#~ "    <tr>\n"
#~ "        <td>freedombox_14_nov_2011.img.xz</"
#~ "td><td>23f83558b41ed2617c741ca7c866b63f</td>\n"
#~ "    </tr>\n"
#~ "    <tr>\n"
#~ "        <td>freedombox_14_nov_2011.img.xz.sig</"
#~ "td><td>404fd39a1ad488b9d3662628a3b5a43b</td>\n"
#~ "    </tr>\n"
#~ "</table>\n"
