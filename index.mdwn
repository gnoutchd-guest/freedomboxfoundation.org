# About FreedomBox
## The Internet Needs Freedom
The Internet is centralized and controlled by a small number of digital titans. That means that it’s easy for the internet to be surveilled, data-mined, and controlled. Think about your own experience of the internet: *can you do anything on the internet without going through a major platform first?*

It’s very difficult to escape the large platforms that control our data and our lives. But it shouldn’t be. That’s why FreedomBox was built: it cuts out the middleman and empowers you to do things like share files, send encrypted messages, have voice calls, and edit documents through a server you host yourself.

## FreedomBox Creates Freedom
FreedomBox is a private server system that empowers regular people to host their own internet services, like a VPN, a personal website, file sharing, encrypted messengers, a VoIP server, a metasearch engine, and much more. It is designed to be secure, flexible, and simple. FreedomBox builds freedom into the internet by putting you in control of your activity and data on the net.

FreedomBox is made with two ingredients: (1) a free software system and (2) always-on, inexpensive, and power-efficient hardware about the size of a pocket dictionary. Though we aim for our software to be hardware-neutral, we specifically support about ten hardware models. All of our supported hardware models are single-board computers that cost about 60 USD and provide the computing power of a smart phone. The software system is 100% free and open source and available for [download at no cost.](https://freedombox.org/) FreedomBox is preloaded with 20+ apps and features designed to protect your freedom, privacy, and user rights.

Just install our system onto your single-board computer and plug it into your internet router. A web interface acts as the central hub, offering one-click installs and simple configuration pages for apps and services.

FreedomBox was born in 2010. After years of development, it is now a fixture in universities, villages, and homes throughout the world. FreedomBox continues to spread [thanks to our donors.](https://freedomboxfoundation.org/donate)

[Learn about the FreedomBox Foundation...](https://freedomboxfoundation.org/foundation)